#include "MIDIUSB.h"
#include "io.h"
#define ARRAYSIZE(x)  (sizeof(x) / sizeof(x[0]))

t_in in[] = {
{ "CrossFad", 1 },
{ "VolFadL", 2 },
{ "VolFadR", 3 },
{ "HiPotL", 4 },
{ "MidPotL", 5 },
{ "BassPotL", 6 },
{ "HiPotR", 7 },
{ "MidPotR", 8 },
{ "BassPotR", 9 },
{ "FilterL", 10 },
{ "FilterR", 11 },
{ "", -1 },
};

input inputs[ARRAYSIZE(in)];

void controlChange(byte channel, byte control, byte value) {
	midiEventPacket_t event = { 0x0B, 0xB0 | channel, control, value };
	MidiUSB.sendMIDI(event);
	MidiUSB.flush(); // Be sure CC is Send
}

void setup()
{
	for (int i = 0; in[i].num != -1; i++) {
		inputs[i] = input(in[i].num);
	}
}

void loop()
{
	for (int i = 0; in[i].num != -1; i++) {
		if (inputs[i].compare(inputs[i].getValue()))
		{
			controlChange(0, i+30, inputs[i].getValue());
		}
	}
}


