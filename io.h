#pragma once
#define _INPUT_THRESHOLD 3

class input
{
private:
	int _pin;
	int _lastValue;
public:
	input();
	input(int pin);
	~input();

	int getValue();
	int compare(int);
};

typedef struct s_in {
	String name;
	int num;
} t_in;