#include "Arduino.h"
#include "io.h"

input::input()
{

}
input::input(int pin)
{
	pinMode(pin, INPUT);
	_pin = pin;
}


input::~input()
{

}


int input::getValue()
{
	return _lastValue = map(analogRead(_pin), 0, 1023, 0, 127);
}


int input::compare(int toComp)
{
	if (abs(_lastValue - toComp) > _INPUT_THRESHOLD) {
		return 1;
	}
	return 0;
}


